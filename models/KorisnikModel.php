<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class KorisnikModel extends Model{
        protected function getFields(): array {
            return [
                'korisnik_id'         => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at'      => new Field((new \App\Validators\DateTimeValidator())->allowDate()->allowTime() , false),

                'korisnicko_ime'        => new Field((new \App\Validators\StringValidator(0, 128)) ),
                'lozinka'   => new Field((new \App\Validators\StringValidator(0, 128)) ),
                'email'           => new Field((new \App\Validators\StringValidator(0, 255)) ),
                'ime'        => new Field((new \App\Validators\StringValidator(0, 64)) ),
                'prezime'         => new Field((new \App\Validators\StringValidator(0, 64)) )
            ];
        }

        public function getByKorisnickoIme(string $korisnickoIme){
            return $this->getAllByFieldName('korisnicko_ime', $korisnickoIme);
        }

        public function getByEmail(string $email){
            return $this->getAllByFieldName('email', $email);
        }
    }