<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class KorisnikPrijavaModel extends Model{
        protected function getFields(): array {
            return [
                'prijava_korisnik_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at'      => new Field((new \App\Validators\DateTimeValidator())->allowDate()->allowTime() , false),

                'korisnik_id'         => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11))
            ];
        }

        public function getByKorisnikId(int $korisnikId) :array{
            return $this->getAllByFieldName('korisnik_id', $korisnikId);
        }
    }