{% extends "_global/index.html" %}

{% block main %} 
{% spaceless %}
<div class="container">
    <div class= "row">
        <div class="col-sm-3 col-md-3 col-lg-3">
        {% for proizvod in proizvodi %}
            <div class="card-body">
                <a href="{{ BASE }}proizvod/{{proizvod.proizvod_id}}" 
                    class="card-title">
                    {{ proizvod.naziv }}
                </a>
            </div>
        
        {% endfor %}
        </div>
<!-- col-12 col-sm-6 col-md-4 col-lg-3 -->
    <div class="col-sm-9 col-md-9 col-lg-9">
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner h-auto">
                <div class="carousel-item active">
                <img src="{{BASE}}assets/img/cvecezarodjendan.jpg" class="d-block m-auto w-100" alt="...">
                </div>
                <div class="carousel-item">
                <img src="{{BASE}}assets/img/cvecezarodjendan.jpg" class="d-block m-auto w-100" alt="...">
                </div>
                <div class="carousel-item">
                <img src="{{BASE}}assets/img/cvecezarodjendan.jpg" class="d-block m-auto w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    </div>
</div>
{% endspaceless%}
{% endblock %}