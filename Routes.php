<?php
    return[
        App\Core\Route::get('|^user/register/?$|',                  'Main',                   'getRegister'),
        App\Core\Route::post('|^user/register/?$|',                 'Main',                   'postRegister'),
        App\Core\Route::get('|^user/login/?$|',                     'Main',                   'getLogin'),
        App\Core\Route::post('|^user/login/?$|',                    'Main',                   'postLogin'),
        App\Core\Route::get('|^user/logout/?$|',                    'Main',                   'getLogout'),
        
        App\Core\Route::get('|^proizvod/([0-9]+)/?$|',              'Proizvod',               'show'),

        App\Core\Route::get('|^oglas/([0-9]+)/?$|',                 'Oglas',               'show'),


        App\Core\Route::get('|^api/oglas/([0-9]+)/?$|',             'ApiOglas',             'show'),
        App\Core\Route::get('|^api/bookmarks/?$|',                  'ApiBookmark',            'getBookmarks'),
        App\Core\Route::get('|^api/bookmarks/add/([0-9]+)/?$|',     'ApiBookmark',            'addBookmark'),
        App\Core\Route::get('|^api/bookmarks/clear/?$|',            'ApiBookmark',            'clear'),
        
        App\Core\Route::get('|^user/profile/?$|',                   'UserDashboard',          'index'),

        App\Core\Route::get('|^user/proizvodi/?$|',                'UserProizvodManagement', 'proizvodi'),
        App\Core\Route::get('|^user/proizvodi/edit/([0-9]+)/?$|',  'UserProizvodManagement', 'getEdit'),
        App\Core\Route::post('|^user/proizvodi/edit/([0-9]+)/?$|', 'UserProizvodManagement', 'postEdit'),
        App\Core\Route::get('|^user/proizvodi/add/?$|',            'UserProizvodManagement', 'getAdd'),
        App\Core\Route::post('|^user/proizvodi/add/?$|',           'UserProizvodManagement', 'postAdd'),

        App\Core\Route::get('|^user/oglasi/?$|',                    'UserOglasManagement', 'oglasi'),
        App\Core\Route::get('|^user/oglasi/edit/([0-9]+)/?$|',      'UserOglasManagement', 'getEdit'),
        App\Core\Route::post('|^user/oglasi/edit/([0-9]+)/?$|',     'UserOglasManagement', 'postEdit'),
        App\Core\Route::get('|^user/oglasi/add/?$|',                'UserOglasManagement', 'getAdd'),
        App\Core\Route::post('|^user/oglasi/add/?$|',               'UserOglasManagement', 'postAdd'),


        App\Core\Route::any('|^.*$|',                               'Main',                   'home')
    ];