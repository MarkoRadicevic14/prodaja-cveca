<?php
    namespace App\Controllers;

    class ApiOglasController extends \App\Core\ApiController {
        public function show($id) {
            $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());
            $oglas = $oglasModel->getById($id);
            $this->set('oglas',  $oglas);
        }
    }
