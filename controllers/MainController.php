<?php
namespace App\Controllers;

class MainController extends \App\Core\Controller{
    

    public function home() {
        $proizvodModel = new \App\Models\ProizvodModel($this->getDatabaseConnection());
        $proizvodi = $proizvodModel->getAll();
        $this->set('proizvodi', $proizvodi);
        
    }

    public function getRegister() {
        
    }

    public function postRegister() {
        $email     = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
        $ime       = \filter_input(INPUT_POST, 'reg_ime', FILTER_SANITIZE_STRING);
        $prezime   = \filter_input(INPUT_POST, 'reg_prezime', FILTER_SANITIZE_STRING);
        $username  = \filter_input(INPUT_POST, 'reg_korisnicko_ime', FILTER_SANITIZE_STRING);
        $password1 = \filter_input(INPUT_POST, 'reg_lozinka_1', FILTER_SANITIZE_STRING);
        $password2 = \filter_input(INPUT_POST, 'reg_lozinka_2', FILTER_SANITIZE_STRING);

        if ($password1 !== $password2) {
            $this->set('message', 'Doslo je do greške: Niste uneli dva puta istu lozinku.');
            return;
        }

        $validanPassword = (new \App\Validators\StringValidator())
            ->setMinLength(7)
            ->setMaxLength(120)
            ->isValid($password1);

        if ( !$validanPassword) {
            $this->set('message', 'Doslo je do greške: Lozinka nije ispravnog formata.');
            return;
        }

        $korisnikModel = new \App\Models\KorisnikModel($this->getDatabaseConnection());

        $korisnik = $korisnikModel->getByFieldName('email', $email);
        if ($korisnik) {
            $this->set('message', 'Doslo je do greške: Već postoji korisnik sa tom adresom e-pošte.');
            return;
        }

        $korisnik = $korisnikModel->getByFieldName('korisnicko_ime', $username);
        if ($korisnik) {
            $this->set('message', 'Doslo je do greške: Već postoji korisnik sa tim korisničkim imenom.');
            return;
        }

        $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

        $korisnikId = $korisnikModel->add([
            'korisnicko_ime'      => $username,
            'lozinka' => $passwordHash,
            'email'         => $email,
            'ime'      => $ime,
            'prezime'       => $prezime
        ]);

        if (!$korisnikId) {
            $this->set('message', 'Doslo je do greške: Nije bilo uspešno registrovanje naloga.');
            return;
        }

        $this->set('message', 'Napravljen je novi nalog. Sada možete da se prijavite.');
    }

    public function getLogin() {

    }

    public function postLogin() {
        $username = \filter_input(INPUT_POST, 'login_korisnicko_ime', FILTER_SANITIZE_STRING);
        $password = \filter_input(INPUT_POST, 'login_lozinka', FILTER_SANITIZE_STRING);

        $validanPassword = (new \App\Validators\StringValidator())
            ->setMinLength(7)
            ->setMaxLength(120)
            ->isValid($password);

        if ( !$validanPassword) {
            $this->set('message', 'Doslo je do greške: Lozinka nije ispravnog formata.');
            return;
        }

        $korisnikModel = new \App\Models\KorisnikModel($this->getDatabaseConnection());

        $korisnik = $korisnikModel->getByFieldName('korisnicko_ime', $username);
        if (!$korisnik) {
            $this->set('message', 'Doslo je do greške: Ne postoji korisnik sa tim korisničkim imenom.');
            return;
        }

        if (!password_verify($password, $korisnik->lozinka)) {
            sleep(1);
            $this->set('message', 'Doslo je do greške: Lozinka nije ispravna.');
            return;
        }

        $this->getSession()->put('korisnik_id', $korisnik->korisnik_id);
        $this->getSession()->save();

        $this->redirect(\Configuration::BASE .'user/profile');
    }

    public function getLogout() {
        $this->getSession()->remove('korisnik_id');
        $this->getSession()->save();
        $this->redirect(\Configuration::BASE);
    }
}