<?php
namespace App\Controllers;

class OglasController extends \App\Core\Controller{
    

    public function show($id) { 
        $oglasModel = new \App\Models\OglasModel($this->getDatabaseConnection());
        $oglas = $oglasModel->getById($id);
        $this->set('oglas',  $oglas);
    }
}